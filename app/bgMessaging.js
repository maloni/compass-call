import RNCallKeep from 'react-native-callkeep';


export default  RemoteMessage = async() => {
    const options = {
         ios: {
             appName: 'MyApp',
             imageName: '../img/TabBar/contacts.imageset/contacts.png',
             ringtoneSound: 'my_ringtone_sound_filename_in_bundle',
             maximumCallGroups: '1',
             maximumCallsPerCallGroup: '1'
         },
         android: {
             alertTitle: 'Permissions Required',
             alertDescription: 'This application needs to access your phone calling accounts to make calls',
             cancelButton: 'Cancel',
             okButton: 'ok',
             imageName: 'sim_icon',
             additionalPermissions: [PermissionsAndroid.PERMISSIONS.READ_CONTACTS]
         }
     };
 
     RNCallKeep.setup(options);
     RNCallKeep.setAvailable(true);
     RNCallKeep.displayIncomingCall(_uuid, "123", "123");
    
 
     return Promise.resolve();
 }
 