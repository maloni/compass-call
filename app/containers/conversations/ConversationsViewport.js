import React, {useContext, useEffect, useState} from 'react'
import {Image, View, Text, Platform, FlatList, TouchableOpacity, BackHandler, Alert} from 'react-native'
import {connect} from 'react-redux'
import CompassContext from '../../context/CompassContext';
import Axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import SingleConversation from './SingleConversation';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import FontAwesome from 'react-native-vector-icons/dist/FontAwesome';


const ConversationsViewport = () => {

  const { marko, setMarko } = useContext(CompassContext)

  const[visible, setVisible] = useState(false)
  const[chatList, setChatList] = useState('');
  const[userNumber, setUserNumber] = useState('');
  const[messages, setMessages] = useState();
  const[recived, setRecived] = useState('');

  useEffect(() => {
    // console.log('CONTEXT'+ JSON.stringify(marko))

    // getData(marko.userData.outboundcid)

    retriveData();

    const backButton = () =>{
      setVisible(false);
      return true;
    }
    

    const backHandler = BackHandler.addEventListener(
      "hardwareBackPress",
      backButton
    );


    return () => backHandler.remove();
  },[])

  const retriveData = async () => {
    try{
      const userStorage = await AsyncStorage.getItem('UserInfoData');
      if(userStorage !==  null){
      const useStorageObj = JSON.parse(userStorage)
      console.log(userStorage)
      console.log('storage' + JSON.stringify(useStorageObj))
      // await this.saveToContext(useStorageObj)
      useStorageObj.message == 'Login Success'
      ? getData(useStorageObj.userData.outboundcid)
      : alert('error')
      }
    } catch(err) {
      console.log(err)
    }
  }

  const getData =async (ext) => {

    const data ='token=031aab510bde1ff367e175fb7e010fb90b60979b'+'&number=1'+ext;

    await Axios.post('http://pbx.compassholding.net/api/v2/getSMS', data)
      .then(res => {

        const chatArray = Object.keys(res.data.data).map(key => ({id: Number(key), number: res.data.data[key]}))

        setChatList(chatArray)
        console.log('Sta je odgovor'+ JSON.stringify(chatArray));
      })
      .catch(err => {
        console.log('error'+err)
      })
      await setUserNumber(ext)


  }

  const getConversation = async (from) => {

    const data ='token=031aab510bde1ff367e175fb7e010fb90b60979b'+'&to=1'+userNumber+'&from='+from;

    await Axios.post('http://pbx.compassholding.net/api/v2/getSMShistory', data)
      .then(res => {
        console.log('sms' + JSON.stringify(res))
        setMessages(res.data.data)
      })
      .catch(err => {
        console.log(err)
      })
    await setVisible(true)
  }

  return (
    <View
      style={{
        flex: 1,
        alignItems: 'center',
        backgroundColor: "#ECEFF1",
        justifyContent: 'center',
        paddingBottom: (Platform.OS === 'ios' ? 50 : 0),
      }}
    >
      {chatList != ''
      ?<View style={{width:'100%', height:'100%', alignItems:'center'}}>
        {!visible
        ?<FlatList
          data={chatList}
          keyExtractor={(item) => item.id}
          renderItem={({ item }) => {
            return(
            <TouchableOpacity
            style={{width:wp('100%'), backgroundColor:'white', padding:wp('3%'), marginTop:2, flexDirection:'row', alignItems:'center'}}
            onPress={() => getConversation(item.number)}
            >
              <FontAwesome name='user-circle-o'  size={wp('7%')} color='#bfbebe' />
              <Text style={{fontSize:wp('4%'), marginLeft:wp('4%')}}>{item.number}</Text>
            </TouchableOpacity>
            )
          }}
        />
        :<SingleConversation messages={messages}/>
        }
      </View>
      :null}
    </View>
  )
}

ConversationsViewport.propTypes = {

}

function mapStateToProps() {
  return {}
}

function mapDispatchToProps() {
  return {}
}

export default connect(mapStateToProps, mapDispatchToProps)(ConversationsViewport)
