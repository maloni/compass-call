import React, { useState } from 'react'
import {View, Text, FlatList, TextInput, TouchableOpacity} from 'react-native'
import FontAwesome from 'react-native-vector-icons/dist/FontAwesome';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

const SingleConversation = ({ messages }) => {

    const [sms, setSms] = useState()

    return(
        <View style={{width:'100%', height:'100%'}}>
            <View style={{height:'90%', width:'100%'}}>
                <FlatList
                    data={messages}
                    keyExtractor={(item) => item.id}
                    renderItem={({ item }) => {

                        const getFlex = () => {
                            if(item.direction == 'outbound'){
                                const outbound = {
                                    flex:'flex-end',
                                    bgColor:'white',
                                    txtColor:'black'
                                }
                                return outbound
                            }
                            else if (item.direction == 'inbound'){
                                const outbound ={
                                    flex:'flex-start',
                                    bgColor:'#3F5057',
                                    txtColor:'white'
                                }
                                return outbound
                            }
                        }

                        return(
                        <View style={{backgroundColor:getFlex().bgColor, marginTop:'2%',padding:'3%', width:'40%', alignSelf:getFlex().flex, borderRadius:10, marginHorizontal:'5%'}}>
                            <Text style={{color:getFlex().txtColor}}>{item.body}</Text>
                        </View>
                        )
                    }}
                />
            </View>
            <View style={{height:'9%', width:'100%', alignSelf:'center', marginTop:'1%', backgroundColor:'white', borderRadius:9, flexDirection:'row', alignItems:'center', paddingHorizontal:wp('3%')}}>
                    <TouchableOpacity>
                        <FontAwesome name='smile-o'  size={wp('7%')} color='#bfbebe' />
                    </TouchableOpacity>
                    <TextInput 
                        value={sms}
                        onChangeText={(text) => setSms(text)}
                        multiline
                        numberOfLines={2}
                        placeholder='Send message ...'
                        style={{marginLeft:wp('3%'), width:'80%'}}
                    />
                    <TouchableOpacity>
                        <FontAwesome name='send-o'  size={wp('7%')} color='#bfbebe' />
                    </TouchableOpacity>   
            </View>
        </View>
    )
}

export default SingleConversation;