import React, { useEffect, useContext, useState } from 'react'
import {Image, View, Text, Platform, FlatList, TouchableOpacity, StyleSheet} from 'react-native'
import {connect} from 'react-redux'
import CompassContext from '../../context/CompassContext';
import Axios from 'axios';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import MaterialIcons from 'react-native-vector-icons/dist/MaterialIcons';
import { getIconType } from 'react-native-elements';
import {makeCall} from '../../modules/pjsip'
import PropTypes from 'prop-types'
import AsyncStorage from '@react-native-community/async-storage';


const HistoryViewport = ({onCallPress}) => {
  
  const { marko, setMarko } = useContext(CompassContext)

  const[callList, setCallList] = useState();

  useEffect(() => {
    // console.log('CONTEXT'+ marko.userData.extension)

    // getData(marko.userData.extension)

    // const data = ''+ + '' + ;

    retriveData()
  },[])

  const retriveData = async () => {
    try{
      const userStorage = await AsyncStorage.getItem('UserInfoData');
      if(userStorage !==  null){
      const useStorageObj = JSON.parse(userStorage)
      console.log(userStorage)
      console.log('storage' + JSON.stringify(useStorageObj))
      // await this.saveToContext(useStorageObj)
      useStorageObj.message == 'Login Success'
      ? getData(useStorageObj.userData.extension)
      : alert('error')
      }
    } catch(err) {
      console.log(err)
    }
  }

  const getData = (ext) => {

    const data ='token=031aab510bde1ff367e175fb7e010fb90b60979b'+'&ext='+ext;

    Axios.post('http://pbx.compassholding.net/api/v2/historyMobileApp', data)
      .then(res => {
        setCallList(res.data.data)
        console.log('lista'+res.data.data)
      })
      .catch(err => {
        console.log('error'+err)
      })

  }

  return (
    <View
      style={{
        flex: 1,
        alignItems: 'center',
        backgroundColor: "#ECEFF1",
        justifyContent: 'center',
        paddingBottom: (Platform.OS === 'ios' ? 50 : 0)
      }}
    >
      <FlatList 
      data={callList}
      keyExtractor={(item) => item.uniqueid}
      showsVerticalScrollIndicator={false}
      renderItem={({ item }) => {
      const getIconType = (status, name, duration, date) => {
        if(status=='propusten'){
          return(
          <View style={styles.callContainer}>
            <MaterialIcons name="call-missed" size={wp('8%')} color="red" />
            <View style={{width:'70%'}}>
              <Text style={{fontSize:wp('5%')}}>{name}</Text>
              <Text>{date}</Text>
            </View>
            <View style={{position:'absolute', right:6}}>
              <Text style={{alignSelf:'center', color:'red'}}>Missed</Text>
            </View>
          </View>
            )
        } else if (status=='primljen'){
          return(
            <View style={styles.callContainer}>
            <MaterialIcons name="call-received" size={wp('8%')} color="orange" />
            <View style={{width:'70%'}}>
              <Text style={{fontSize:wp('5%')}}>{name}</Text>
              <Text>{date}</Text>
            </View>
            <View style={{position:'absolute', right:6}}>
              <Text style={{alignSelf:'center'}}>{duration}</Text>
            </View>
          </View>
          )
        } else if (status=='odlazni'){
          return(
            <View style={styles.callContainer}>
            <MaterialIcons name="call-made" size={wp('8%')} color="green" />
            <View style={{width:'70%'}}>
              <Text style={{fontSize:wp('5%')}}>{name}</Text>
              <Text>{date}</Text>
            </View>
            <View style={{position:'absolute', right:6}}>
              <Text style={{alignSelf:'center'}}>{duration}</Text>
            </View>
          </View>
          )
        } else if (status=='zauzet'){
          return(
            <View style={styles.callContainer}>
            <MaterialIcons name="call-missed-outgoing" size={wp('8%')} color="red" />
            <View style={{width:'70%'}}>
              <Text style={{fontSize:wp('5%')}}>{name}</Text>
              <Text>{date}</Text>
            </View>
            <View style={{position:'absolute', right:6}}>
              <Text style={{alignSelf:'center', color:'red'}}>Busy</Text>
            </View>
          </View>
          )
        }

      }

        return(
          <TouchableOpacity 
          onPress={() => onCallPress(item.phone)}
          style={{width:wp('90%'), marginTop:hp('1%'), backgroundColor:'white'}}>
            {getIconType(item.statusCall, item.name, item.duration, item.date)}
          </TouchableOpacity>
        )}}
      />
    </View>
  )
}

const styles=StyleSheet.create({
  callContainer:{
    width:'100%',
    flexDirection:'row',
    alignItems:'center',
    paddingVertical:hp('1%'),
    paddingHorizontal:wp('1%')
  }
})

HistoryViewport.propTypes = {
  onCallPress: PropTypes.func.isRequired
}

function mapStateToProps() {
  return {}
}

function mapDispatchToProps(dispatch) {
  return {
    onCallPress: (destination) => {
      if (destination) {
        dispatch(makeCall(destination))
      }
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(HistoryViewport)
