import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import HomeScreen from './HomeScreen';
import AppScreen from './containers/AppScreen';

const NewNavigator = createStackNavigator({

    home: {screen: HomeScreen},
    appScreen : {screen: AppScreen}

  });
  
  export default createAppContainer(NewNavigator);
  