import React, { Component } from 'react'
import { Animated, View, StyleSheet, Text, TouchableOpacity } from 'react-native'
import Touchable from '../../common/Touchable'
import PropTypes from 'prop-types'
import s from './styles'
import FontAwesome from 'react-native-vector-icons/dist/FontAwesome';
import Feather from 'react-native-vector-icons/dist/Feather';
import Ionicons from 'react-native-vector-icons/dist/Ionicons';

function getNameOfTab(tab) {
  switch (tab) {
    case 'dialer':
      return 'Dialer'
    case 'conversations':
      return 'Chat'
    case 'history':
      return 'History'
    case 'settings':
      return 'Settings'
  }
}

class NavigationItem extends Component {
  constructor(props) {
    super(props)

    this.state = {
      offsetValue: new Animated.Value(props.offset),
      opacityValue: new Animated.Value(props.selected ? 1 : 0)
    }

    this.props.onPress = this.props.onPress.bind(this)
    console.log("this.props");
    console.log(this.props);
  }

  componentDidUpdate(nextProps) {
    if (this.props.selected !== nextProps.selected) {
      Animated.timing(this.state.opacityValue, { toValue: nextProps.selected ? 1 : 0, duration: 300 }).start()
    }
    if (this.props.offset !== nextProps.offset) {
      Animated.timing(this.state.offsetValue, { toValue: nextProps.offset, duration: 300 }).start()
    }
  }

  onPress() {
    console.log(this.props)
    const { tab, onPress } = this.props
    onPress && onPress(tab)
  }

  icons (tab) {
    getNameOfTab(tab) 
      switch (tab) {
        case 'dialer':
          return <Feather name='phone-call' size={30} color='white' />
        case 'conversations':
          return <Feather name='message-square' size={30} color='white' />
        case 'history':
          return <FontAwesome name='history' size={30} color='white' />
        case 'settings':
          return <Feather name='settings' size={30} color='white' />
      }

}

  render() {
    const { tab, onTextLayout } = this.props

    return (
      <View style={styles.container}>
        <TouchableOpacity onPress={()=>this.onPress()} style={s.touchable} >
          <View style={{alignSelf:'center'}}>{this.icons(tab)}</View>
        <Text style={styles.text} numberOfLines={1}>
          {getNameOfTab(tab)}
        </Text>
        </TouchableOpacity>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
      top: 7,
      flexDirection: 'row',
      height: 75,
      alignSelf:'center',
      width:'25%'
    },
  text: {
      paddingTop: 8,
      fontWeight: 'bold',
      color: "#FFF",
      fontSize:16,
      alignSelf:'center'
  },
  touchable: {
      height: 36,
      borderColor:'red',
  }
})

NavigationItem.propTypes = {
  tab: PropTypes.string,
  selected: PropTypes.bool,
  offset: PropTypes.number,
  onTextLayout: PropTypes.func,
  onPress: PropTypes.func
}

export default NavigationItem