import React, {Component, useState, useEffect} from 'react'
import {View, StatusBar, Text, TextInput, TouchableOpacity, Alert, Linking, AppState, Image, Platform} from 'react-native';
import AppScreen from '../app/containers/AppScreen';
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import * as Navigation from '../app/modules/navigation'
import {createAccount, deleteAccount} from '../app/modules/pjsip'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import axios from 'react-native-axios';
import CompassContext from '../app/context/CompassContext';
import messaging from '@react-native-firebase/messaging';
import FontAwesome from 'react-native-vector-icons/dist/FontAwesome';
import InCallManager from 'react-native-incall-manager';
import AsyncStorage from '@react-native-community/async-storage';


messaging().setBackgroundMessageHandler(async remoteMessage => {

  Linking.openURL('chrome//app')

});


class HomeScreen extends Component {

  static contextType = CompassContext

    constructor(props) {
        super(props)
        

          this.state = {
            appState: AppState.currentState,
            addable: false,
            usernameUser:'',
            saveUser:false,
            passwordUser:'',
            userInfo:'',
            visible:false,
            // marko: true,
            name: "Marko",
            username: "8107",
            domain: "pbx.compassholding.net:5060",
            password: "markomaloni1991",
    
            proxy: "",
            transport: "TCP",
            regServer: "",
            regTimeout: ""
          }          
          this._onUsernameChanged = this.onFieldChanged.bind(this, "username")
          this._onPasswordChanged = this.onFieldChanged.bind(this, "password")      
          this._onSubmitPress = this.onSubmitPress.bind(this)
          this._onDeletePress = this.onDeletePress.bind(this)
      }
                
      async componentDidMount(){
        await this.retriveData();
        await messaging()
          .getToken()
          .then(token => {
            console.log('token  ' +token)
          })

          console.log('accc' + this.props.account)
          
      }


      saveToContext = async (acc) => {
           const { marko, setMarko } = this.context

           await setMarko(acc)
           await console.log('test context' + JSON.stringify(marko))

      }

      onFieldChanged = (name, value) => {
        const s = {...this.state, [name]: value}
        const addable = s.name.length > 0 && s.username.length > 0 && s.domain.length > 0 && s.password.length > 0
    
        this.setState({[name]: value, addable: addable})
      }
    
      async onSubmitPress(data){
    
        const credentials = {
            name: data.userData.displayname,
            username: data.userData.extension,
            domain: "pbx.compassholding.net:5060",
            password: data.userData.secret,
    
            proxy: "",
            transport: "TCP",
            regServer: "",
            regTimeout: ""
        }
        await this.props.onCreatePress && this.props.onCreatePress(credentials)
        await this.setState({visible:true})

            
      }

      saveUser = async (data) => {
        try{
        await AsyncStorage.setItem('UserInfoData', JSON.stringify(data))
        await this.saveToContext(data)
        } catch(err){
          console.log(err)
        }
      }

      retriveData = async () => {
        try{
          const userStorage = await AsyncStorage.getItem('UserInfoData');
          if(userStorage !==  null){
          const useStorageObj = JSON.parse(userStorage)
          console.log(userStorage)
          console.log('storage' + JSON.stringify(useStorageObj))
          await this.saveToContext(useStorageObj)
          useStorageObj.message == 'Login Success'
          ? this.onSubmitPress(useStorageObj)
          : alert('error')
          }
        } catch(err) {
          console.log(err)
        }
      }

      checkAndSave = async (data) => {
        await this.saveUser(data)
        await this.onSubmitPress(data)
        // await Navigation.goAndReplace('settings')
        await this.setState({visible: true})
      }

      loginReq = () => {
        const user ='email='+this.state.usernameUser+'&password='+this.state.passwordUser
        console.log('6435'+user)

        axios.post('http://pbx.compassholding.net/api/v2/loginApp', user)
          .then(res => {

            console.log('odgovor' + JSON.stringify(res.data))

            console.log(res.data.userData.extension)
            console.log(res.data.userData.secret)

            const data = res.data
            
            data.message == 'Login Success'
            ?this.checkAndSave(data)
            :alert(res.data.message)
          })
          .catch(err => {
            console.log(err)
          })
      }
    
      onDeletePress() {
        this.props.onDeletePress && this.props.onDeletePress(this.props.account)
      }
    
      render() {

        console.log('accc' + JSON.stringify(this.props.accounts))

        return (
        <>
          {   typeof this.props.account == undefined  
              ?<View style={{alignItems:'center', width:wp('100%')}}>
                <View style={{height:hp('15%'), alignSelf:'center', width:wp('80%')}}>
                <Image style={{width:'100%', height:'100%'}} resizeMode='contain' source={require('../app/assets/images/CHlogo.png')} />
                </View>
                  <View style={{width:wp('60%'), borderBottomWidth:1, flexDirection:'row', alignItems:'center', borderColor:'#a9a9a9', marginTop:hp('15%')}}>
                    <FontAwesome name='user' size={wp('6.5%')} color='#a9a9a9' />
                    <TextInput 
                      style={{marginLeft:wp('2%')}}
                      placeholder='Username'
                      onChangeText={(text) => this.setState({usernameUser: text})}
                      value={this.state.usernameUser}
                  />
                  </View>
                  <View style={{width:wp('60%'), borderBottomWidth:1, flexDirection:'row', alignItems:'center', borderColor:'#a9a9a9', marginTop:hp('2%')}}>
                    <FontAwesome name='lock' size={wp('6.5%')} color='#a9a9a9' />
                    <TextInput 
                    secureTextEntry={true}
                    style={{marginLeft:wp('2%')}}
                    placeholder='Password'
                    onChangeText={(text) => this.setState({passwordUser: text})}
                    value={this.state.passwordUser}
                    />
                  </View>
                  <TouchableOpacity
                  onPress={() => this.loginReq()}
                  style={{width:wp('40%'), borderWidth:1, height:hp('6%'), marginTop:hp('5%'), borderRadius:7, alignItems:'center', justifyContent:'center', marginTop:hp('10%')}}
                  >
                    <Text style={{fontWeight:'500', fontSize:wp('4%')}}>Login</Text>
                  </TouchableOpacity>
              </View>
              :<AppScreen />
          }
        </>
    )
  }
}

  HomeScreen.propTypes = {
    account: PropTypes.shape({
      getName: PropTypes.func,
      getUsername: PropTypes.func,
      getDomain: PropTypes.func,
      getPassword: PropTypes.func,
      getProxy: PropTypes.func,
      getTransport: PropTypes.func,
      getRegServer: PropTypes.func,
      getRegTimeout: PropTypes.func
    }),
    onBackPress: PropTypes.func,
    onCreatePress: PropTypes.func,
    onChangePress: PropTypes.func,
    onDeletePress: PropTypes.func,
    accounts: PropTypes.object,
  }


  function select(store) {
    return {
        account: store.navigation.current.account,
        accounts: store.pjsip.accounts
    }
  }
  
  function actions(dispatch) {
    return {
      onBackPress: () => {
        dispatch(Navigation.goBack())
      },
      onCreatePress: (configuration) => {
        dispatch(async () => {
          await dispatch(createAccount(configuration))
          await dispatch(Navigation.goAndReplace({name: 'dialer'}))
        })
      },
      onChangePress: (account, configuration) => {
        alert("Not implemented")
        dispatch(Navigation.goAndReplace({name: 'settings'}))
        // dispatch(replaceAccount(account, configuration));
      },
      onDeletePress: (account) => {
        dispatch(deleteAccount(account))
      },
    }
  }



export default connect(select, actions)(HomeScreen);
