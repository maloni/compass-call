import React,{ useState, useEffect } from 'react';
import { AsyncStorage } from 'react-native';

const CompassContext = React.createContext();

export const CompassProvider = ({ children }) => {
    const [itaId, setItaId] = useState('marko');
    const [marko, setMarko] = useState({})
    const [callStatus, setCallStatus] = useState('')


    console.log(itaId)
    return <CompassContext.Provider value={{itaId,setItaId, marko, setMarko,callStatus, setCallStatus}}>
        {children}
    </CompassContext.Provider>
};

export default CompassContext;
